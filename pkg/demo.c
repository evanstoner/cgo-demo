#include "demo.h"
#include <string.h>

CONTAINER_T getContainer() {
  CONTAINER_T container;
  container.theChar = '-';
  container.theInt = 6;
  container.theLong = 7;
  container.theFloat = 8.9;
  container.theDouble = 10.11;
  strcpy(container.theString, "twelve thirteen");
  return container;
}

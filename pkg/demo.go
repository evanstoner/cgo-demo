package demo

//#include "demo.h"
import "C"

type Container struct {
	TheChar   uint8
	TheInt    int
	TheLong   int32
	TheFloat  float32
	TheDouble float64
	TheString string
}

func GetContainer() Container {
	var cContainer C.CONTAINER_T = C.getContainer()
	return Container{
		TheChar:   uint8(cContainer.theChar),
		TheInt:    int(cContainer.theInt),
		TheLong:   int32(cContainer.theLong),
		TheFloat:  float32(cContainer.theFloat),
		TheDouble: float64(cContainer.theDouble),
		TheString: C.GoString(&cContainer.theString[0]),
	}
}

typedef struct _container {
  char theChar;
  int theInt;
  long theLong;
  float theFloat;
  double theDouble;
  char theString[20];
} CONTAINER_T;

CONTAINER_T getContainer();

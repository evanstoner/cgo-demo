package main

import (
	"gitlab.com/evanstoner/cgo-demo/pkg"

	"fmt"
)

func main() {
	goContainer := demo.GetContainer()
	fmt.Println(goContainer)
}
